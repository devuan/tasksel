# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Kartik Mistry <kartik.mistry@gmail.com>, 2006,2007.
#
msgid ""
msgstr ""
"Project-Id-Version: tasksel-debian-po-gu\n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2006-09-13 14:42-0400\n"
"PO-Revision-Date: 2006-10-06 13:37+0530\n"
"Last-Translator: Kartik Mistry <kartik.mistry@gmail.com>\n"
"Language-Team: Gujarati <team@utkarsh.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Description
#. Type: multiselect
#. Description
#: ../templates:1001 ../templates:2001
msgid "Choose software to install:"
msgstr "સ્થાપન કરવા માટે સોફ્ટવેર પસંદ કરો:"

#. Type: multiselect
#. Description
#: ../templates:1001
msgid ""
"At the moment, only the core of the system is installed. To tune the system "
"to your needs, you can choose to install one or more of the following "
"predefined collections of software."
msgstr ""
"આ પરિસ્થિતિમાં, ફક્ત સિસ્ટમનો મુખ્ય ભાગ જ સ્થાપિત થશે. તમારી જરૂરિયાત મુજબ "
"સિસ્ટમને ગોઠવવા માટે, તમે નીચેનાં એક અથવા વધુ પહેલેથી નક્કી કરેલ સોફ્ટવેરનાં ભાગો "
"સ્થાપિત કરી શકો છો."

#. Type: multiselect
#. Description
#: ../templates:2001
msgid ""
"You can choose to install one or more of the following predefined "
"collections of software."
msgstr ""
"તમે નીચેનાં એક અથવા વધુ પહેલેથી નક્કી કરેલ સોફ્ટવેરનાં ભાગો સ્થાપિત કરી શકો છો."

#. Type: title
#. Description
#: ../templates:3001
msgid "Software selection"
msgstr "સોફ્ટવેર પસંદગી"
